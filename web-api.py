from flask import Flask, request, jsonify

app = Flask(__name__)

def ok():
    return jsonify({'success': True})

def error(error):
    return jsonify({'success': False, 'error': error})

def execute(data):
    print('params : ' + str(data))
    # name = data.get('name')
    # 此处调用任务方法或者执行脚本
    return ok()

@app.route('/', methods=['POST'])
def handle():
    try:
        return execute(request.json)
    except Exception as e:
        return error(str(e))

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

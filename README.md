### 环境

##### python 版本

python 3.x

##### 安装 Flask 、 gunicorn 模块

```
pip install Flask
pip install gunicorn
```

### 启动

```
nohup gunicorn -w 4 -b 0.0.0.0:5000 web-api:app > web-api.log 2>&1 &
```

### 停止

```
ps aux | grep gunicorn
kill <PID>
```

### 访问

```
curl --location 'http://127.0.0.1:5000' \
--header 'Content-Type: application/json' \
--data '{
    "name": "1222"
}'
```

### 修改端口

```
app.run(host='0.0.0.0', port=xxxx)
nohup gunicorn -w 4 -b 0.0.0.0:xxxx web-api:app > web-api.log 2>&1 &

```



